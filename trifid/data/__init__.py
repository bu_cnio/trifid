from __future__ import absolute_import, division, print_function

__author__ = "Fernando Pozo"
__copyright__ = "Copyright 2021"
__license__ = "GNU General Public License"
__version__ = "1.0.0"
__maintainer__ = "Fernando Pozo"
__email__ = "fpozoc@cnio.es"
__status__ = "Development"