#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(name='trifid',
      version='1.0.0',
      description='Tool for Reliable Idenfication of Functional Isoform Data.',
      long_description='''
      New computational approach to classify Alternative Splicing isoforms. 
      This machine learning approach uses almost 50 different features from 5 distinct categories to make 
      its predictions. The algorithm classified functional isoforms with high confidence and in contrast 
      to previous attempts to predict functional alternative isoforms. It predicts a high proportion of
      distinct protein isoforms do have not a functional role in the cell.
      ''',
      author='Fernando Pozo',
      author_email='fpozoc@gmx.com',
      url='https://gitlab.com/fpozoc',
      download_url='https://gitlab.com/bu_cnio/trifid/-/archive/master/trifid-master.tar.gz',
      license='GNU General Public License',
      install_requires=[
            'biopython', 
            'numpy',
            'pandas>=1', 
            'scikit-learn', 
            'joblib',
            'cython',
            'pyyaml',
            'gtfparse',
            'loguru',
            'muscle'
            ],
      extras_require={
            'extra': [
                  'matplotlib',
                  'altair',
                  'altair_saver',
                  'eli5',
                  'shap',
                  'mlxtend',
                  'rfpimp'
            ],
            'interactive': [
                  'jupyterlab',
                  'watermark'
            ],
      },
      package_data={
            'config': ['config/config.yaml'],
            'features': ['config/features.yaml'],
      },
      packages=find_packages())